<?php
/**
 * Naxero.com Magento 2 Payment module (https://www.naxero.com)
 *
 * Copyright (c) 2017 Naxero.com (https://www.naxero.com)
 * Author: David Fiaty | contact@naxero.com
 *
 * License GNU/GPL V3 https://www.gnu.org/licenses/gpl-3.0.en.html
 */

namespace Naxero\Mercanet\Controller\Shopper;
 
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Customer\Model\Session as CustomerSession;
use Naxero\Mercanet\Helper\Tools;

class SessionData extends Action
{
    protected $customerSession;
    protected $tools;

    public function __construct(Context $context, CustomerSession $customerSession, Tools $tools)
    {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->tools = $tools;
   }
 
    public function execute()
    {
        // Get the request data
        $sessionData = $this->tools->getInputData();

        // Save in session
        $this->customerSession->setData('checkoutSessionData', $sessionData);

        // End the script
        exit();
    }
}