<?php
/**
 * Naxero.com Magento 2 Payment module (https://www.naxero.com)
 *
 * Copyright (c) 2017 Naxero.com (https://www.naxero.com)
 * Author: David Fiaty | contact@naxero.com
 *
 * License GNU/GPL V3 https://www.gnu.org/licenses/gpl-3.0.en.html
 */

namespace Naxero\Mercanet\Controller\Response;
 
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Naxero\Mercanet\Helper\Tools;
use Naxero\Mercanet\Model\Service\OrderService;
use Naxero\Mercanet\Gateway\Config\Config;

class Automatic extends Action
{
    protected $tools;
    protected $orderService;
    protected $config;
 
    public function __construct(Context $context, Tools $tools, OrderService $orderService, Config $config)
    {
        parent::__construct($context);
        $this->tools = $tools;
        $this->orderService = $orderService;
        $this->config = $config;
    }
 
    public function execute()
    {
        // Get the request data
        $responseData = $this->tools->getInputData();

        // Check validity
        if ($this->tools->isValid($responseData, $this->config->getSecretKey())) {       
            // Place order
            $this->orderService->gatewayCallback($responseData);

            // Stop the execution
            exit();
        }
    }
}