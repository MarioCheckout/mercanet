<?php
/**
 * Naxero.com Magento 2 Payment module (https://www.naxero.com)
 *
 * Copyright (c) 2017 Naxero.com (https://www.naxero.com)
 * Author: David Fiaty | contact@naxero.com
 *
 * License GNU/GPL V3 https://www.gnu.org/licenses/gpl-3.0.en.html
 */

namespace Naxero\Mercanet\Helper;

use Magento\Framework\Message\ManagerInterface;
use Naxero\Mercanet\Gateway\Config\Config;
use Naxero\Mercanet\Helper\Tools;

class Watchdog {

    protected $messageManager;
    protected $config;
    protected $tools;

    public function __construct(ManagerInterface $messageManager, Config $config, Tools $tools) {
        $this->messageManager = $messageManager;
        $this->config = $config;
        $this->tools = $tools;
    }

    public function bark($msg = null, $data = null) {
        // Default message
        if ($msg) {
            $this->messageManager->addNoticeMessage($msg);
        }

        // Debug messsages
        if ($this->config->isDebugMode() && $data) {
            // Add the response code
            if (isset($data['responseCode'])) {
                $this->messageManager->addNoticeMessage(__('Response code') . ' : ' .  $data['responseCode']);
            }         
        }
    }

    public function log($msg, $data = null) {
        // Prepare the output
        $output  = $msg . "\n";
        $output .= ($data) ? print_r($data, 1) : '';

        // Write to log file
        $logFile = BP . '/var/log/' . $this->tools->modmeta['tag'] . '.log';
        $writer = new \Zend\Log\Writer\Stream($logFile);
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($output);
    }    
}
