<?php
/**
 * Naxero.com Magento 2 Payment module (https://www.naxero.com)
 *
 * Copyright (c) 2017 Naxero.com (https://www.naxero.com)
 * Author: David Fiaty | contact@naxero.com
 *
 * License GNU/GPL V3 https://www.gnu.org/licenses/gpl-3.0.en.html
 */
 
namespace Naxero\Mercanet\Gateway\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Naxero\Mercanet\Helper\Tools;

class Config {

    const KEY_MODTAG = 'modtag';
    const KEY_ENVIRONMENT = 'environment';
    const KEY_ACTIVE = 'active';
    const KEY_DEBUG = 'debug';
    const KEY_SIMU_MERCHANT_ID = 'simu_merchant_id';
    const KEY_TEST_MERCHANT_ID = 'test_merchant_id';
    const KEY_PROD_MERCHANT_ID = 'prod_merchant_id';
    const KEY_KEY_VERSION = 'key_version';
    const KEY_INTERFACE_VERSION = 'interface_version';
    const KEY_COUNTRY_CREDIT_CARD = 'countrycreditcard';
    const KEY_INTEGRATION = 'integration';
    const KEY_CAPTURE_MODE = 'capture_mode';
    const KEY_CAPTURE_TIME = 'capture_time';
    const KEY_VERIFY_3DSECURE = 'verify_3dsecure';
    const KEY_SIMU_API_URL = 'simu_api_url';
    const KEY_TEST_API_URL = 'test_api_url';
    const KEY_PROD_API_URL = 'prod_api_url';
    const KEY_SIMU_SECRET_KEY = 'simu_secret_key';
    const KEY_TEST_SECRET_KEY = 'test_secret_key';
    const KEY_PROD_SECRET_KEY = 'prod_secret_key';
    const KEY_NORMAL_RETURN_URL = 'normal_return_url';
    const KEY_AUTOMATIC_RESPONSE_URL = 'automatic_response_url';
    const KEY_BUTTON_LABEL = 'button_label';
    const CODE_3DSECURE = 'three_d_secure';
    const KEY_NEW_ORDER_STATUS = 'new_order_status';
    const KEY_ORDER_STATUS_AUTHORIZED = 'order_status_authorized';
    const KEY_ORDER_STATUS_CAPTURED = 'order_status_captured';
    const KEY_ORDER_STATUS_REFUNDED = 'order_status_refunded';
    const KEY_ORDER_STATUS_FLAGGED = 'order_status_flagged';
    const KEY_ACCEPTED_CURRENCIES = 'accepted_currencies';
    const KEY_PAYMENT_CURRENCY = 'payment_currency';
    const KEY_CUSTOM_CURRENCY = 'custom_currency';
    const KEY_AUTO_GENERATE_INVOICE = 'auto_generate_invoice';
    const KEY_INVOICE_CREATION = 'invoice_creation';
    const KEY_ORDER_CREATION = 'order_creation';
    const IMMEDIATE_CAPTURE = 'IMMEDIATE';
    const DEFERRED_CAPTURE = 'AUTHOR_CAPTURE';
    const BYPASS_3DS = 'ALL';

    protected $scopeConfig;
    protected $tools;

    public function __construct(ScopeConfigInterface $scopeConfig, Tools $tools) {
        $this->scopeConfig = $scopeConfig;
        $this->tools = $tools;
    }

    public function getValue($path) {
        return $this->scopeConfig->getValue('payment/' . $this->tools->modmeta['tag'] . '/' . $path);
    }

	private static $currencies = array(
        'EUR' => '978', 'USD' => '840', 'CHF' => '756', 'GBP' => '826',
        'CAD' => '124', 'JPY' => '392', 'MXP' => '484', 'TRY' => '949',
        'AUD' => '036', 'NZD' => '554', 'NOK' => '578', 'BRC' => '986',
        'ARP' => '032', 'KHR' => '116', 'TWD' => '901', 'SEK' => '752',
        'DKK' => '208', 'KRW' => '410', 'SGD' => '702', 'XPF' => '953',
        'XOF' => '952'
    );

    /**
     * Returns the merchant ID.
     *
     * @return string
     */
    public function getMerchantId() {
        // Get the environement
        $mode = $this->getEnvironement();

        // Return the gateway url
        switch($mode) {
            case 'simu': 
            $id = $this->getValue(self::KEY_SIMU_MERCHANT_ID);
            break;

            case 'test': 
            $id = $this->getValue(self::KEY_TEST_MERCHANT_ID);
            break;

            case 'prod': 
            $id = $this->getValue(self::KEY_PROD_MERCHANT_ID);
            break;

            default:
            $id = $this->getValue(self::KEY_SIMU_MERCHANT_ID);
        }

        return (string) $id;
    }

    /**
     * Returns the key version.
     *
     * @return string
     */
    public function getKeyVersion() {
        return (string) $this->getValue(self::KEY_KEY_VERSION);
    }

    /**
     * Returns the transaction data.
     *
     * @return string
     */
    public function getRequestData($quote, $currency) {
        // Prepare the currency code
        $currencyCode = $this->convertCurrencyToCurrencyCode($currency);

        // Prepare the amount
        $amount = $quote->getGrandTotal()*100;

        // Order id
        $orderId = $quote->reserveOrderId()->save()->getReservedOrderId();

        //$transactionReference = ;
        $transactionReference = time();

        // Prepare the parameters array
        $params = array(
            'amount' => $amount,
            'currencyCode' => $currencyCode,
            'merchantId' => $this->getMerchantId(),
            'customerId' => $quote->getCustomerId(),
            'normalReturnUrl' => $this->getNormalReturnUrl(),
            'automaticResponseUrl' => $this->getAutomaticResponseUrl(),
            'transactionReference' => $transactionReference,
            'customerEmail' => $quote->getCustomerEmail(),
            'orderId' => $orderId,
            'keyVersion' => $this->getKeyVersion(),
            'captureMode' => $this->getCaptureMode(),
            'captureDay' => $this->getCaptureDay()
        );

        // Prepare 3Ds settings
        if (!$this->isVerify3DSecure()) {
            $params['fraudData.bypass3DS'] = self::BYPASS_3DS;
        }

        // Prepare the parameters string
        $str = [];
        foreach ($params as $key => $value) {
            $str[] = $key . '=' . $value;
        }
        $params = implode('|', $str);

        // Prepare the seal
        $seal = hash('sha256', $params . $this->getSecretKey());

        return array(
            'params' => $params,
            'seal' => $seal
        );
    }

    public function getCaptureMode() {
        return (string) ($this->isAutoCapture()) ? self::IMMEDIATE_CAPTURE : self::DEFERRED_CAPTURE;       
    }

    public function getCaptureDay() {       
        return (int) ($this->isAutoCapture()) ?  0 : $this->getValue(self::KEY_CAPTURE_TIME);
    }

    public function convertCurrencyToCurrencyCode($currency)
    {
        if(!in_array($currency, array_keys(self::$currencies)))
            throw new InvalidArgumentException("Unknown currencyCode $currency.");
        return self::$currencies[$currency];
    }

    /**
     * Returns the button label.
     *
     * @return string
     */
    public function getButtonLabel() {
         return (string) $this->getValue(self::KEY_BUTTON_LABEL);   
    }

    /**
     * Returns the interface version.
     *
     * @return string
     */
    public function getInterfaceVersion() {
        return (string) $this->getValue(self::KEY_INTERFACE_VERSION);
    }

    /**
     * Returns the normal return URL.
     *
     * @return string
     */
    public function getNormalReturnUrl() {
        // Object manager
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();     

        // Store manager   
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');   

        // Scope config
        $scopeConfig = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface');

        return (string) $storeManager->getStore()->getBaseUrl() . $this->tools->modmeta['tag'] . '/' . $this->getValue(self::KEY_NORMAL_RETURN_URL);
    }

    /**
     * Returns the automatic response URL.
     *
     * @return string
     */
    public function getAutomaticResponseUrl() {
        // Object manager
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();        

        // Store manager   
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');

        // Scope config
        $scopeConfig = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface');
        
        return (string) $storeManager->getStore()->getBaseUrl() . $this->tools->modmeta['tag'] . '/' . $this->getValue(self::KEY_AUTOMATIC_RESPONSE_URL);
    }

    /**
     * Returns the automatic invoice generation state.
     *
     * @return bool
     */
    public function getAutoGenerateInvoice() {
        return (bool) $this->getValue(self::KEY_AUTO_GENERATE_INVOICE);
    }

    /**
     * Returns the invoice creation mode.
     *
     * @return bool
     */     
    public function getInvoiceCreationMode() {
        return (string) $this->getValue(self::KEY_INVOICE_CREATION);
    }

    /**
     * Returns the new order status.
     *
     * @return string
     */
    public function getNewOrderStatus() {
        return (string) $this->getValue(self::KEY_NEW_ORDER_STATUS);
    }

    /**
     * Returns the authorized order status.
     *
     * @return string
     */
    public function getOrderStatusAuthorized() {
        return (string) $this->getValue(self::KEY_ORDER_STATUS_AUTHORIZED);
    }

    /**
     * Returns the captured order status.
     *
     * @return string
     */
    public function getOrderStatusCaptured() {
        return (string) $this->getValue(self::KEY_ORDER_STATUS_CAPTURED);
    }

    /**
     * Returns the refunded order status.
     *
     * @return string
     */
    public function getOrderStatusRefunded() {
        return (string) $this->getValue(self::KEY_ORDER_STATUS_REFUNDED);
    }
    
    /**
     * Determines the environment.
     *
     * @return bool
     */
    public function getEnvironement() {
        return (string) $this->getValue(self::KEY_ENVIRONMENT);
    }

    /**
     * Returns the type of integration.
     *
     * @return string
     */
    public function getIntegration() {
        return (string) $this->getValue(self::KEY_INTEGRATION);
    }

    /**
     * Determines if the gateway is active.
     *
     * @return bool
     */
    public function isActive() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $quote = $objectManager->create('Magento\Checkout\Model\Session')->getQuote();   
        return true;  
        // todo - improve code here and check duplicate with isActive in payment method facade
        //return (bool) in_array($quote->getQuoteCurrencyCode(), $this->getAcceptedCurrencies());
    }

    /**
     * Determines if Apple Pay is active.
     *
     * @return bool
     */
    public function isActiveApplePay() {
        return true;
    }

    /**
     * Determines if debug mode is enabled.
     *
     * @return bool
     */
    public function isDebugMode() {
        return (bool) $this->getValue(self::KEY_DEBUG);
    }

    /**
     * Returns the active secret key.
     *
     * @return string
     */
    public function getSecretKey() {
        $key = $this->getSimuSecretKey();
        $mode = $this->getEnvironement();

        switch($mode) {
            case 'simu': 
            $url = $this->getSimuSecretKey();

            case 'test': 
            $url = $this->getTestSecretKey();

            case 'prod': 
            $url = $this->getProdSecretKey();
        }

        return (string) $key;
    }

    /**
     * Returns the simulation secret key for client-side functionality.
     *
     * @return string
     */
    public function getSimuSecretKey() {
        return (string) $this->getValue(self::KEY_SIMU_SECRET_KEY);
    }

    /**
     * Returns the test secret key for client-side functionality.
     *
     * @return string
     */
    public function getTestSecretKey() {
        return (string) $this->getValue(self::KEY_TEST_SECRET_KEY);
    }

    /**
     * Returns the production secret key for client-side functionality.
     *
     * @return string
     */
    public function getProdSecretKey() {
        return (string) $this->getValue(self::KEY_PROD_SECRET_KEY);
    }

    /**
     * Determines if 3D Secure option is enabled.
     *
     * @return bool
     */
    public function isVerify3DSecure() {
        return (bool) $this->getValue(self::KEY_VERIFY_3DSECURE);
    }

    /**
     * Returns the currencies allowed for payment.
     *
     * @return array
     */
    public function getAcceptedCurrencies() {
        return (array) explode(',', $this->getValue(self::KEY_ACCEPTED_CURRENCIES));
    }

    /**
     * Returns the payment currency.
     *
     * @return string
     */
    public function getPaymentCurrency() {
        return (string) $this->getValue(self::KEY_PAYMENT_CURRENCY);
    }

    /**
     * Returns the custom payment currency.
     *
     * @return string
     */
    public function getCustomCurrency() {
        return (string) $this->getValue(self::KEY_CUSTOM_CURRENCY);
    }

    /**
     * Returns the API URL for simulation environment.
     *
     * @return string
     */
    public function getSimuApiUrl() {
        return (string) $this->getValue(self::KEY_SIMU_API_URL);
    }

    /**
     * Returns the API URL for test environment.
     *
     * @return string
     */
    public function getTestApiUrl() {
        return (string) $this->getValue(self::KEY_TEST_API_URL);
    }

    /**
     * Returns the API URL for production environment.
     *
     * @return string
     */
    public function getProdApiUrl() {
        return (string) $this->getValue(self::KEY_PROD_API_URL);
    }

    /**
     * Returns the redirect URL.
     *
     * @return string
     */
    public function getRedirectUrl() {
        // Get the environement
        $mode = $this->getEnvironement();

        // Return the gateway url
        switch($mode) {
            case 'simu': 
            $url = $this->getSimuApiUrl();
            break;

            case 'test': 
            $url = $this->getTestApiUrl();
            break;

            case 'prod': 
            $url = $this->getProdApiUrl();
            break;

            default:
            $url = $this->getSimuApiUrl();
        }

        return (string) $url;
    }

    /**
     * Returns the new order creation setting.
     *
     * @return string
     */
    public function getOrderCreation() {
        return (string) $this->getValue(self::KEY_ORDER_CREATION);
    }

    /**
     * Determines if auto capture option is enabled.
     *
     * @return bool
     */
    public function isAutoCapture() {
        return (bool) ($this->getValue(self::KEY_CAPTURE_MODE)) == self::IMMEDIATE_CAPTURE;
    }

    /**
     * Return the country specific card type config.
     *
     * @return array
     */
    public function getCountrySpecificCardTypeConfig() {
        $countriesCardTypes = unserialize($this->getValue(self::KEY_COUNTRY_CREDIT_CARD));
        return is_array($countriesCardTypes) ? $countriesCardTypes : [];
    }

    /**
     * Get list of card types available for country.
     *
     * @param string $country
     * @return array
     */
    public function getCountryAvailableCardTypes($country) {
        $types = $this->getCountrySpecificCardTypeConfig();
        return (!empty($types[$country])) ? $types[$country] : [];
    }
}
