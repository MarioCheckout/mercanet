<?php
/**
 * Naxero.com Magento 2 Payment module (https://www.naxero.com)
 *
 * Copyright (c) 2017 Naxero.com (https://www.naxero.com)
 * Author: David Fiaty | contact@naxero.com
 *
 * License GNU/GPL V3 https://www.gnu.org/licenses/gpl-3.0.en.html
 */

use Magento\Framework\Component\ComponentRegistrar;
use Naxero\Mercanet\Helper\Tools;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Naxero_Mercanet', __DIR__);
