<?php
/**
 * Naxero.com Magento 2 Payment module (https://www.naxero.com)
 *
 * Copyright (c) 2017 Naxero.com (https://www.naxero.com)
 * Author: David Fiaty | contact@naxero.com
 *
 * License GNU/GPL V3 https://www.gnu.org/licenses/gpl-3.0.en.html
 */
 
namespace Naxero\Mercanet\Block\Adminhtml\System\Config\Field;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Backend\Block\Template\Context;
use Naxero\Mercanet\Helper\Tools;

class NormalReturnUrl extends Field {

    const KEY_URL = 'normal_return_url';

    protected $tools;

    /**
     * @param Context $context
     * @param Tools $tools
     * @param array $data
     */
    public function __construct(
        Context $context,
        Tools $tools,
        array $data = []
    ) {
        $this->tools = $tools;
        parent::__construct($context, $data);
    }

    /**
     * Overridden method for rendering a field. In this case the field must be only for read.
     *
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element) {
        $callbackUrl = $this->getBaseUrl() . $this->tools->modmeta['tag'] . '/' . $this->getControllerUrl();

        $element->setData('value', $callbackUrl);
        $element->setReadonly('readonly');

        return $element->getElementHtml();
    }
    
    /**
     * Returns the controller url.
     *
     * @return string
     */
    public function getControllerUrl() {
        return $this->_scopeConfig->getValue('payment/' . $this->tools->modmeta['tag'] . '/' . self::KEY_URL);        
    }
}
