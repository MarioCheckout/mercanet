/**
 * Naxero.com Magento 2 Payment module (https://www.naxero.com)
 *
 * Copyright (c) 2017 Naxero.com (https://www.naxero.com)
 * Author: David Fiaty | contact@naxero.com
 *
 * License GNU/GPL V3 https://www.gnu.org/licenses/gpl-3.0.en.html
 */

define(
    [
        'uiComponent',
        'Naxero_Mercanet/js/view/payment/adapter',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function(
        Component,
        Adapter,
        rendererList
    ) {
        'use strict';

        var paymentMethod = window.checkoutConfig.payment[Adapter.getCode()];
        var paymentMethodApplePay = window.checkoutConfig.payment[Adapter.getCodeApplePay()];

        if (paymentMethod.isActive) {
            rendererList.push({
                type: Adapter.getCode(),
                component: Adapter.getName() + '/js/view/payment/method-renderer/default'
            });
        }

        if (paymentMethodApplePay.isActive) {
            rendererList.push({
                type: Adapter.getCodeApplePay(),
                component: Adapter.getName() + '/js/view/payment/method-renderer/apple-pay'
            });
        }

        return Component.extend({});
    }
);