/**
 * Naxero.com Magento 2 Payment module (https://www.naxero.com)
 *
 * Copyright (c) 2017 Naxero.com (https://www.naxero.com)
 * Author: David Fiaty | contact@naxero.com
 *
 * License GNU/GPL V3 https://www.gnu.org/licenses/gpl-3.0.en.html
 */

define([
    'jquery',
    'Magento_Ui/js/model/messageList'
], function($, globalMessageList) {
    'use strict';

    return {

        /**
         * Get payment code.
         * @returns {String}
         */
        getCode: function() {
            return window.checkoutConfig.payment['modtag'];
        },

        /**
         * Get payment code.
         * @returns {String}
         */
        getCodeApplePay: function() {
            return window.checkoutConfig.payment['modtagapplepay'];
        },

        /**
         * Get payment name.
         * @returns {String}
         */
        getName: function() {
            return window.checkoutConfig.payment['modname'];
        },

        /**
         * Get payment configuration array.
         * @returns {Array}
         */
        getPaymentConfig: function() {
            return window.checkoutConfig.payment[this.getCode()];
        },

        /**
         * Show error message
         *
         * @param {String} errorMessage
         */
        showError: function(errorMessage) {
            globalMessageList.addErrorMessage({
                message: errorMessage
            });
        }

    };

});