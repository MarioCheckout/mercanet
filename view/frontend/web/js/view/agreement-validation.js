/**
 * Naxero.com Magento 2 Payment module (https://www.naxero.com)
 *
 * Copyright (c) 2017 Naxero.com (https://www.naxero.com)
 * Author: David Fiaty | contact@naxero.com
 *
 * License GNU/GPL V3 https://www.gnu.org/licenses/gpl-3.0.en.html
 */

define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Naxero_Mercanet/js/model/agreement-validator',
    ],
    function(Component, additionalValidators, agreementValidator) {
        'use strict';
        additionalValidators.registerValidator(agreementValidator);
        return Component.extend({});
    }
);