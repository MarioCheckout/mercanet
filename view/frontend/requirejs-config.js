/**
 * Naxero.com Magento 2 Payment module (https://www.naxero.com)
 *
 * Copyright (c) 2017 Naxero.com (https://www.naxero.com)
 * Author: David Fiaty | contact@naxero.com
 *
 * License GNU/GPL V3 https://www.gnu.org/licenses/gpl-3.0.en.html
 */

var config = {
    map: {
        '*': {
            'Magento_Checkout/js/model/place-order': 'Naxero_Mercanet/js/model/place-order',
            'Magento_Checkout/js/model/error-processor': 'Naxero_Mercanet/js/model/error-processor',
        }
    }
};