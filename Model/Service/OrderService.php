<?php
/**
 * Naxero.com Magento 2 Payment module (https://www.naxero.com)
 *
 * Copyright (c) 2017 Naxero.com (https://www.naxero.com)
 * Author: David Fiaty | contact@naxero.com
 *
 * License GNU/GPL V3 https://www.gnu.org/licenses/gpl-3.0.en.html
 */

namespace Naxero\Mercanet\Model\Service;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Customer\Api\Data\GroupInterface;
use Magento\Quote\Model\QuoteManagement;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Naxero\Mercanet\Gateway\Config\Config as GatewayConfig;
use Naxero\Mercanet\Helper\Tools;
use Naxero\Mercanet\Helper\Watchdog;
use Naxero\Mercanet\Model\Service\TransactionService;
use Naxero\Mercanet\Model\Service\InvoiceHandlerService;

class OrderService {

    /**
     * @var TransactionService
     */
    protected $transactionService;

    /**
     * @var InvoiceHandlerService
     */
    protected $invoiceHandlerService;

    /**
     * @var QuoteManagement
     */
    protected $quoteManagement;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var GatewayConfig
     */
    protected $gatewayConfig;

    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var OrderSender
     */
    protected $orderSender;

    /**
     * @var Tools
     */
    protected $tools;

    /**
     * @var Watchdog
     */
    protected $watchdog;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepository;

    /**
     * CallbackService constructor.
     * @param TransactionService $transactionService
     * @param InvoiceHandlerService $invoiceHandlerService
     * @param CheckoutSession $checkoutSession
     * @param GatewayConfig $gatewayConfig
     * @param JsonFactory $resultJsonFactory
     * @param OrderSender $orderSender
     * @param Tools $tools
     * @param Watchdog $watchdog
     * @param OrderRepositoryInterface $orderRepository
     * @param CartRepositoryInterface $cartRepository
     */
    public function __construct(
        TransactionService $transactionService,
        InvoiceHandlerService $invoiceHandlerService,
        CheckoutSession $checkoutSession,
        GatewayConfig $gatewayConfig,
        CustomerSession $customerSession,
        QuoteManagement $quoteManagement, 
        JsonFactory $resultJsonFactory,
        OrderSender $orderSender,
        Tools $tools,
        Watchdog $watchdog,
        OrderRepositoryInterface $orderRepository,
        CartRepositoryInterface $cartRepository                        
    ) {
        $this->transactionService = $transactionService;
        $this->invoiceHandlerService = $invoiceHandlerService;
        $this->checkoutSession       = $checkoutSession;
        $this->customerSession       = $customerSession;
        $this->quoteManagement       = $quoteManagement;
        $this->resultJsonFactory     = $resultJsonFactory;
        $this->gatewayConfig         = $gatewayConfig;
        $this->orderSender           = $orderSender;
        $this->tools                 = $tools;
        $this->watchdog              = $watchdog;
        $this->orderRepository       = $orderRepository;
        $this->cartRepository        = $cartRepository;
    }

    /**
     * Handles the controller method.
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function placeOrder($data) {
        // Load the customer quote
        $quote = $this->checkoutSession->getQuote();

        // Get the fields
        $fields = $this->tools->unpackData($data['Data'], '|', '=');

        // If the transaction is not successfull
        if ($fields['responseCode'] != '00') {
            $this->watchdog->bark(__('The transaction could not be processed'), $fields);
            return false;
        }

        // If the quote is valid
        if (isset($fields['orderId']) && $quote->getReservedOrderId() == $fields['orderId'] && ($quote)) {
            // Check for guest email
            if ($quote->getCustomerEmail() === null
                && $this->customerSession->isLoggedIn() === false
                && isset($this->customerSession->getData('checkoutSessionData')['customerEmail'])
            ) 
            {
                // Prepare the quote
                $quote->setCustomerId(null)
                ->setCustomerEmail($this->customerSession->getData('checkoutSessionData')['customerEmail'])
                ->setCustomerIsGuest(true)
                ->setCustomerGroupId(GroupInterface::NOT_LOGGED_IN_ID)
                ->save();
            }

            // Prepare session quote info for redirection after payment
            $this->checkoutSession->setLastQuoteId($quote->getId())
            ->setLastSuccessQuoteId($quote->getId())
            ->clearHelperData();

            // Set up the payment information
            $payment = $quote->getPayment();
            $payment->setMethod($this->tools->modmeta['tag']);

            // Create the order
            $order = $this->quoteManagement->submit($quote);

            // Process the order
            return $this->processOrder($order, $fields);              
        }

        return false;
    }

    /**
     * Handles the callback method.
     *
     */
    public function gatewayCallback($data) {
        // Get the fields
        $fields = $this->tools->unpackData($data['Data'], '|', '=');

        // If the quote exists
        if ((int) $fields['customerId'] > 0) {
            // Get the quote
            $quote = $this->cartRepository->getActiveForCustomer($fields['customerId']);

            // If the transaction is not successfull
            if ($fields['responseCode'] != '00') {
                $this->watchdog->log(__('The transaction could not be processed'), $fields);
                return false;
            }            
        }

        return false;
    }

    private function processOrder($order, $fields) {
        try {
            // Format the gateway amount
            $amount = $this->tools->formatAmount($fields['amount']);

            // Update order status
            if ($this->gatewayConfig->isAutocapture()) {
                // Update order status
                $order->setStatus($this->gatewayConfig->getOrderStatusCaptured());
                $this->orderRepository->save($order);

                // Create the transaction
                $transactionId = $this->transactionService->createTransaction($order, $fields, 'capture');
            }
            else {
                // Update order status
                $order->setStatus($this->gatewayConfig->getOrderStatusAuthorized());

                // Create the transaction
                $transactionId = $this->transactionService->createTransaction($order, $fields, 'authorization');
            }

            // Create the invoice
            $this->invoiceHandlerService->processInvoice($order);

            // Send the email
            $this->orderSender->send($order);
            $order->setEmailSent(1);

            // Prepare session order info for redirection after payment
            $this->checkoutSession->setLastOrderId($order->getId())
            ->setLastRealOrderId($order->getIncrementId())
            ->setLastOrderStatus($order->getStatus());

            // Save the order
            $this->orderRepository->save($order);

            return true;
        }
        catch (Exception $e) {
            $this->watchdog->bark(__($e->getMessage()));
            return false;
        }        
    }
}