<?php
/**
 * Naxero.com Magento 2 Payment module (https://www.naxero.com)
 *
 * Copyright (c) 2017 Naxero.com (https://www.naxero.com)
 * Author: David Fiaty | contact@naxero.com
 *
 * License GNU/GPL V3 https://www.gnu.org/licenses/gpl-3.0.en.html
 */

namespace Naxero\Mercanet\Model\Adminhtml\Source;

use Magento\Framework\Option\ArrayInterface;

class CaptureMode implements ArrayInterface {

    const MODE_1 = 'IMMEDIATE';
    const MODE_2 = 'AUTHOR_CAPTURE';
    const MODE_3 = 'VALIDATION';

    /**
     * Possible environment types
     *
     * @return array
     */
    public function toOptionArray() {
        return [
            [
                'value' => self::MODE_1,
                'label' => __('Immediate'),
            ],
            [
                'value' => self::MODE_2,
                'label' => __('Deferred'),
            ],
            [
                'value' => self::MODE_3,
                'label' => __('Validation'),
            ],
        ];
    }

}
