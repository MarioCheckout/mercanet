<?php
/**
 * Naxero.com Magento 2 Payment module (https://www.naxero.com)
 *
 * Copyright (c) 2017 Naxero.com (https://www.naxero.com)
 * Author: David Fiaty | contact@naxero.com
 *
 * License GNU/GPL V3 https://www.gnu.org/licenses/gpl-3.0.en.html
 */

namespace Naxero\Mercanet\Model\Adminhtml\Source;

use Magento\Framework\Option\ArrayInterface;

class OrderCreation implements ArrayInterface {

    const BEFORE_AUTH = 'before_auth';
    const AFTER_AUTH = 'after_auth';

    /**
     * Possible environment types
     *
     * @return array
     */
    public function toOptionArray() {
        return [
            [
                'value' => self::BEFORE_AUTH,
                'label' => 'Before authorization'
            ],
            [
                'value' => self::AFTER_AUTH,
                'label' => 'After authorization'
            ],    
        ];
    }

}