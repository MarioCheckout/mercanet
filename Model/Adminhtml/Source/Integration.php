<?php
/**
 * Naxero.com Magento 2 Payment module (https://www.naxero.com)
 *
 * Copyright (c) 2017 Naxero.com (https://www.naxero.com)
 * Author: David Fiaty | contact@naxero.com
 *
 * License GNU/GPL V3 https://www.gnu.org/licenses/gpl-3.0.en.html
 */

namespace Naxero\Mercanet\Model\Adminhtml\Source;

use Magento\Framework\Option\ArrayInterface;

class Integration implements ArrayInterface {

    const INTEGRATION_POST = 'POST';
    const INTEGRATION_JSON = 'JSON';

    /**
     * Possible environment types
     *
     * @return array
     */
    public function toOptionArray() {
        return [
            [
                'value' => self::INTEGRATION_POST,
                'label' => __('POST'),
            ],
            [
                'value' => self::INTEGRATION_JSON,
                'label' => __('JSON'),
            ],    
        ];
    }

}