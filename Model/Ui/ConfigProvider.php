<?php
/**
 * Naxero.com Magento 2 Payment module (https://www.naxero.com)
 *
 * Copyright (c) 2017 Naxero.com (https://www.naxero.com)
 * Author: David Fiaty | contact@naxero.com
 *
 * License GNU/GPL V3 https://www.gnu.org/licenses/gpl-3.0.en.html
 */

namespace Naxero\Mercanet\Model\Ui;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Checkout\Model\Session;
use Magento\Checkout\Model\Cart;
use Magento\Store\Model\StoreManagerInterface;
use Naxero\Mercanet\Gateway\Config\Config;
use Naxero\Mercanet\Helper\Tools;

class ConfigProvider implements ConfigProviderInterface {

    const CODE = 'naxero_mercanet';
    const CODE_APPLE_PAY = 'naxero_mercanet_apple_pay';

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Tools
     */
    protected $tools;

    /**
     * ConfigProvider constructor.
     * @param Config $config
     * @param Session $checkoutSession
     * @param Cart $cart
     * @param StoreManagerInterface $storeManager
     * @param Tools $tools
     */
    public function __construct(Config $config, Session $checkoutSession, Cart $cart, StoreManagerInterface $storeManager, Tools $tools) {
        $this->config = $config;
        $this->checkoutSession = $checkoutSession;
        $this->cart = $cart;
        $this->storeManager = $storeManager;
        $this->tools = $tools;
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig() {
        $quote    = $this->cart->getQuote()->collectTotals()->save();
        $quoteCurrency = $this->storeManager->getStore()->getCurrentCurrencyCode();

        return [
            'payment' => [
                'modtag' => $this->tools->modmeta['tag'],
                'modtagapplepay' => $this->tools->modmeta['tagapplepay'],
                'modname' => $this->tools->modmeta['name'],
                $this->tools->modmeta['tag'] => [
                    'isActive'                  => $this->config->isActive(),
                    'debug_mode'                => $this->config->isDebugMode(),
                    'redirect_url'              => $this->config->getRedirectUrl(),
                    'interface_version'         => $this->config->getInterfaceVersion(),
                    'request_data'              => $this->config->getRequestData($quote, $quoteCurrency),
                    'button_label'              => $this->config->getButtonLabel(),
                    '3dsenabled'                => $this->config->isVerify3DSecure(),
                    'integration'               => $this->config->getIntegration(),
                    'order_creation'            => $this->config->getOrderCreation(),
                ],
                $this->tools->modmeta['tagapplepay'] => [
                    'isActive'          => $this->config->isActiveApplePay(),
                ],            
            ],
        ];
    }
}
